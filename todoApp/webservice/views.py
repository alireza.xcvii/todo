from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from todoApp.models import ToDo
import json

@method_decorator(csrf_exempt, name='dispatch')
class AddTodo(View):
    def post(self, request, *args, **kwargs):
        if request.POST:
            data = request.POST.get('data')
            loaded_data = json.load(data)

            object =loaded_data.get("message")

            title = object.get('title')
            text = object.get('text')

            # title = request.POST.get('title')
            # text = request.POST.get('text')

            todo = ToDo.objects.create(
                title=title,
                text=text,
            )
            todo.save()




class Todos(View):
    def get(self, request):
        todos = ToDo.objects.all()

        result = []
        for task in todos:
            t = {"title":task.title,"text":task.text}
            result.append(t)

        task_dict = {
            "tasks":result
        }
        return JsonResponse(task_dict)