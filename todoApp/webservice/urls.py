from django.urls import path
from todoApp.webservice.views import AddTodo,Todos
from . import views

urlpatterns = [
    path('', AddTodo.as_view() ,name='add'),
    path('get/', Todos.as_view() ,name='get'),
]
