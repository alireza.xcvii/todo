from django.shortcuts import render
from django.http import HttpResponse

from .models import ToDo

def index(request):

    todos = ToDo.objects.all()

    todos_dict = {
        'todos' : todos
    }

    return render(request,'index.html',todos_dict)


def detail(request, id):

    todo = ToDo.objects.get(id=id)

    todo_dict = {
        'todo' : todo
    }

    return render(request, 'detail.html', todo_dict)


