# Generated by Django 2.0.5 on 2018-07-18 13:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todoApp', '0002_auto_20180718_1331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todo',
            name='priority',
            field=models.IntegerField(),
        ),
    ]
