from django.db import models
from datetime import datetime

class ToDo(models.Model):
    title = models.CharField(max_length=128)
    text = models.TextField()
    #created_at = models.DateTimeField(default=datetime.now)
    # must_be_don_at = models.DateTimeField(null=True)
    # priority = models.IntegerField(null=True)

    def __str__(self):
        return self.title
